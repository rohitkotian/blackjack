//
//  AppUtils.swift
//  BlackJack
//
//  Created by Rohit Kotian on 2/1/15.
//  Copyright (c) 2015 Ethical Games. All rights reserved.
//

import UIKit

func DLog(object: Any, function: String = __FUNCTION__, line: Int = __LINE__, fileName : String = __FILE__) {
    #if DEBUG
        Swift.println("\(fileName.lastPathComponent) \(line) \(function) :\(object)")
    #endif
}

func ALog(object: Any, function: String = __FUNCTION__, line: Int = __LINE__, fileName : String = __FILE__) {
    Swift.println("\(fileName.lastPathComponent) \(line) \(function) :\(object)")
}

extension Array {
    var last: T {
        return self[self.endIndex - 1]
    }
}

func == <T:Equatable> (tuple1:(T,T),tuple2:(T,T)) -> Bool {
    return (tuple1.0 == tuple2.0) && (tuple1.1 == tuple2.1)
}

func > (lhs:(Int, Int), rhs:(Int, Int)) -> Bool {
    if ((lhs.0 > rhs.0) || (lhs.1 > rhs.1)) {
        return true
    }
    return false
}

func + (tuple1:(Int, Int),tuple2:(Int, Int)) -> (Int, Int) {
    return ((tuple1.0 + tuple2.0), (tuple1.1 + tuple2.1))
}

extension UIImage {
    class func image(suit : Suit, value : Value) -> UIImage? {
        return UIImage(named: "\(suit.description)_\(value.description)")
    }
}