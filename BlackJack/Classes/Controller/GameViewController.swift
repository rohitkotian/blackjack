//
//  GameViewController.swift
//  BlackJack
//
//  Created by Rohit Kotian on 1/31/15.
//  Copyright (c) 2015 Ethical Games. All rights reserved.
//

import UIKit

class GameViewController: UIViewController {
    let gameManager = GameManager()
    
    @IBOutlet weak var lblRoundNumber: UILabel!
    @IBOutlet weak var lblHandValue: UILabel!
    @IBOutlet weak var lblBusted: UILabel!
    @IBOutlet weak var btnStartAgain: UIButton!
    @IBOutlet weak var lblDealerHandValue: UILabel!
    
    var canSplit = false
    let firstCard : Card!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        gameManager.deal({ (roundNumber) -> Void in
            self.lblRoundNumber.text = "\(roundNumber)"
            if (self.gameManager.isBlackjackForPlayer()) {
                self.lblBusted.hidden = false
                self.lblBusted.text = "Dealer is busted"
                self.btnStartAgain.enabled = true
            }
        }, dealtCardHandler: { (player, card, forSplit) -> Void in
            self.updateCard(player, card: card, isDealing: true, forSplit: forSplit)
        }) { (player) -> Void in
            self.canSplit = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func onHit(sender: UIButton) {
        //TODO: Update UI
        gameManager.hit({ (player) -> Void in
            // dealer win's
        }, dealtCardHandler: { (player, card, forSplit) -> Void in
            self.updateCard(player, card: card, isDealing: false, forSplit: forSplit)
        })
    }
    
    @IBAction func onStand(sender: UIButton) {
        gameManager.stand({ (player) -> Void in
            // TODO: - Check who win!? and update UI
        }, tieHandler: { (player) -> Void in
            // It's a tie
        }) { (player, card, forSplit) -> Void in
            self.updateCard(player, card: card, isDealing: false, forSplit: forSplit)
        }
    }
    
    
    @IBAction func onSplit(sender: UIButton) {
        if canSplit {
            gameManager.split()
        }
    }
    
    @IBAction func onStartAgain(sender: UIButton) {
        self.lblBusted.hidden = true
//        self.btnStartAgain.enabled = false
        self.lblHandValue.text = "0"
        self.lblBusted.text = "GG Your Busted!"
        gameManager.startNewRound()
        gameManager.deal({ (roundNumber) -> Void in
            self.lblRoundNumber.text = "\(roundNumber)"
        }, dealtCardHandler: { (player, card, forSplit) -> Void in
            self.updateCard(player, card: card, isDealing: true, forSplit: forSplit)
        }) { (player) -> Void in
            
        }
    }
    
    
    // MARK: - PRIVATE FUNCTIONS FOR UI UPDATES
    private func updateCard(player : BJPlayer, card : Card, isDealing : Bool, forSplit : Bool) -> Void {
        // UPDATE CARD STACK
        // UPDATE SCORE
        DLog("\(player.ID) was dealt card \(card) ~> score \(player.bjScore(forSplit))")
    }
    
    private func clearStack() {
        
    }
    
    private func reshuffle() {
        
    }
}
