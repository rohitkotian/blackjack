//
//  PlayerBO.swift
//  BlackJack
//
//  Created by Rohit Kotian on 2/1/15.
//  Copyright (c) 2015 Ethical Games. All rights reserved.
//

import Foundation

class PlayerBO : NSObject {
    
    class var sharedInstance : PlayerBO {
        struct Static {
            static let instance : PlayerBO = PlayerBO()
        }
        return Static.instance
    }
    
    func addCard(player : BJPlayer, card : Card) {
        player.hand.append(card)
    }
    
}
