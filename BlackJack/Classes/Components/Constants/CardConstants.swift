//
//  CardConstants.swift
//  BlackJack
//
//  Created by Rohit Kotian on 1/31/15.
//  Copyright (c) 2015 Ethical Games. All rights reserved.
//


enum Suit : Int, Printable {
    case Clubs = 1, Diamonds, Hearts, Spades
    
    static let values = [Clubs, Diamonds, Hearts, Spades]
    
    // To match with image assets
    var description : String {
        get {
            switch(self) {
            case .Clubs:
                return "club"
            case .Diamonds:
                return "Diamond"
            case .Hearts:
                return "Heart"
            case .Spades:
                return "spade"
            }
        }
    }
}

enum Value : Int, Printable {
    case Ace = 1, Two, Three, Four, Five, Six, Seven, Eigth, Nine, Ten, Jack, Queen, King
    
    static let values = [Ace, Two, Three, Four, Five, Six, Seven, Eigth, Nine, Ten, Jack, Queen, King]
    
    // To match with image assets
    var description : String {
        get {
            switch(self) {
            case .Ace:
                return "A"
            case .Two:
                return "2"
            case .Three:
                return "3"
            case .Four:
                return "4"
            case .Five:
                return "5"
            case .Six:
                return "6"
            case .Seven:
                return "7"
            case .Eigth:
                return "8"
            case .Nine:
                return "9"
            case .Ten:
                return "10"
            case .Jack:
                return "J"
            case .Queen:
                return "Q"
            case .King:
                return "K"
            }
        }
    }
}