//
//  GameManager.swift
//  BlackJack
//
//  Created by Rohit Kotian on 1/31/15.
//  Copyright (c) 2015 Ethical Games. All rights reserved.
//

import UIKit

public class GameManager: NSObject {
    
    public func testCase(a : Int, b : Int) -> Int {
        return a + b
    }
    
    typealias DealCompletionHandler = (roundNumber : Int) -> Void
    typealias DealtCardHandler = (player : BJPlayer, card : Card, forSplit : Bool) -> Void
    typealias SplitHandler = (player : BJPlayer) -> Void
    typealias WinHandler = (player : BJPlayer) -> Void
    typealias TieHandler = (player : BJPlayer) -> Void
    
    var roundNumber = 0
    var playerBO = PlayerBO.sharedInstance
    var deck : Deck!
    var dealer : BJPlayer = BJPlayer(ID: "0", name: "Rohit", isBot: true)
    var player : BJPlayer = BJPlayer(ID: "1", name: "Sameer", isBot: false)
    var processSplit : Bool = false
    
    override public init() {
        super.init()
        initDeck()
    }
    
    // This is the intial deal, when the game starts
    func deal(dealCompletion : DealCompletionHandler, dealtCardHandler : DealtCardHandler! = nil, splitHandler : SplitHandler) {
        ++roundNumber
        
        // CARD DEALING FOR PLAYER
        let firstCard = Card(suit: .Diamonds, value: .Two)
        player.addCard(firstCard)
        dealtCardHandler(player: player, card: firstCard, forSplit: false)
        var card = Card(suit: .Clubs, value: .Two)
        player.addCard(card)
        if (firstCard.value == card.value) {
            splitHandler(player: player)
        }
        dealtCardHandler(player: player, card: card, forSplit: false)
        
        // CARD DEALING FOR DEALER
        card = randomCard()
        dealer.addCard(card)
        dealtCardHandler(player: dealer, card: card, forSplit: false)
        card = randomCard()
        dealer.addCard(card)
        dealtCardHandler(player: dealer, card: card, forSplit: false)
        
        dealCompletion(roundNumber: roundNumber)
    }
    
    //TODO: - Pass index for player for mutiple player blackjack
    func isBlackjackForPlayer() -> Bool {
        return player.hand.score() == BLACKJACK_LIMIT.1
    }
    
    func hit(winHandler : WinHandler, dealtCardHandler : DealtCardHandler) {
        var card = randomCard()
        player.addCard(card, forSplit: processSplit)
        dealtCardHandler(player: player, card: card, forSplit: processSplit)
        if (player.bjScore(processSplit) > BLACKJACK_LIMIT.0) {
            winHandler(player: dealer)
            if processSplit {
                DLog("SPLIT : player looses...");
            } else {
                DLog("player looses...");
                processSplit = true
            }
        }
    }
    
    //TODO: If player and dealer score is the same, start a new round
    //TODO: Check ace cases...
    func stand(winHandler : WinHandler, tieHandler : TieHandler, dealtCardHandler : DealtCardHandler) {
        if processSplit {
            while (dealer.bjScore() < SOFT_LIMIT.0) {
                let card = randomCard()
                dealer.addCard(card)
                dealtCardHandler(player: dealer, card: card, forSplit: false)
                if (dealer.bjScore() > BLACKJACK_LIMIT.0) {
                    // delaer is busted
                    DLog("dealer looses, player WINS!")
                    winHandler(player: player)
                    return;
                }
            }
            if player.hasSplit() {
                if (dealer.bjScore() == player.bjScore(processSplit)) {
                    DLog("SPLIT: its a tie")
                    tieHandler(player: player)
                } else if (dealer.bjScore() > player.bjScore(processSplit)) {
                    DLog("SPLIT: Dealer wins")
                    winHandler(player: dealer)
                } else {
                    DLog("SPLIT: player wins!")
                    winHandler(player: player)
                }
            }
            if (dealer.bjScore(false) == player.bjScore(false)) {
                DLog("its a tie")
                tieHandler(player: player)
            } else if (dealer.bjScore(false) > player.bjScore(false)) {
                DLog("Dealer wins")
                winHandler(player: dealer)
            } else {
                DLog("player wins!")
                winHandler(player: player)
            }
        } else {
            processSplit = true
        }
    }
    
    func split() {
        DLog("Splitting...")
        player.split()
    }
    
    func doubleDown() {
        
    }
    
    func startNewRound() {
        processSplit = false
        player.reset()
        dealer.reset()
    }
    
    func randomCard() -> Card {
        let deckSize = self.deck.deckSize()
        if (deckSize == 0) {
            self.deck.redeck()
        }
        return self.deck.randomCard()
    }
    
    // MARK: - PRIVATE FUNCTIONS
    private func initDeck() {
        self.deck = DeckFactory.deck()
    }
    
    private func isBusted(values : Int ...) -> Bool {
        for value in values {
            if value <= 21 {
                return false
            }
        }
        return true
    }
    
}
