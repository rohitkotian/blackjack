//
//  DeckFactory.swift
//  BlackJack
//
//  Created by Rohit Kotian on 1/31/15.
//  Copyright (c) 2015 Ethical Games. All rights reserved.
//

struct DeckFactory {
    
    static func deck() -> Deck {
        return Deck(cards: CardFactory.cards())
    }
    
    static func deck(size : Int) -> Deck {
        var cards : [Card] = []
        for index  in 0...(size-1) {
            cards += CardFactory.cards();
        }
        return Deck(cards: cards)
    }
    
}
