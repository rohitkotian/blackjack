//
//  CardFactory.swift
//  BlackJack
//
//  Created by Rohit Kotian on 1/31/15.
//  Copyright (c) 2015 Ethical Games. All rights reserved.
//

struct CardFactory {
    
    static func cards() -> [Card] {
        var cards : [Card] = []
        for suit in Suit.values {
            for value in Value.values {
                cards.append(Card(suit: suit, value: value))
            }
        }
        return cards
    }
    
}