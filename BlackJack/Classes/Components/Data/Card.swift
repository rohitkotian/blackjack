//
//  Card.swift
//  BlackJack
//
//  Created by Rohit Kotian on 1/31/15.
//  Copyright (c) 2015 Ethical Games. All rights reserved.
//

import UIKit

class Card: NSObject, Printable {
    
    var suit : Suit
    var value : Value
    
    var asset : UIImage? {
        get {
            return UIImage.image(suit, value : value)
        }
    }
    
    init(suit : Suit, value : Value) {
        self.suit = suit
        self.value = value
    }
    
    func scoreValue() -> (Int, Int) {
        switch (self.value) {
        case .Ace:
            return (1, 11)
        case .King, .Queen, .Jack:
            return (10, 10)
        default:
            return (self.value.rawValue, self.value.rawValue)
        }
    }
    
    override var description : String {
        get {
            return "\(suit) ~ \(value)"
        }
    }
    
}

protocol CardOperations {
    func add(card : Card) -> (Int, Int)
    class func add(cards : [Card]) -> (Int, Int)
}

extension Card : CardOperations {
    
    func add(card : Card) -> (Int, Int) {
        return self.scoreValue() + card.scoreValue()
    }
    
    class func add(cards: [Card]) -> (Int, Int) {
        var _scoreValue : (Int, Int) = (0, 0)
        for card in cards {
            _scoreValue = _scoreValue + card.scoreValue()
        }
        return _scoreValue
    }
}
