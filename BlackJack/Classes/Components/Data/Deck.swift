//
//  Deck.swift
//  BlackJack
//
//  Created by Rohit Kotian on 1/31/15.
//  Copyright (c) 2015 Ethical Games. All rights reserved.
//

import UIKit

class Deck: NSObject {
    var cards : [Card]
    var usedCards : [Card] = []
    
    init(cards : [Card]) {
        self.cards = cards
    }
    
    func randomCard() -> Card {
        let index = randomCardIndex()
        var randomCard = self.cards.removeAtIndex(index)
        usedCards.append(randomCard)
        return randomCard
    }
    
    func deckSize() -> UInt32 {
        return UInt32(self.cards.count)
    }
    
    func redeck() {
        assert(self.cards.count == 0, "REDECK ONLY ON '0' card count")
        self.cards = usedCards
        usedCards = []
    }
    
    func redeck(cards : [Card]) {
        self.cards = cards
    }
    
    // PRIVATE FUNCTIONS
    private func randomCardIndex() -> Int {
        return Int(arc4random_uniform(deckSize()))
    }
    
}
