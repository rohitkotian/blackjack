//
//  Player.swift
//  BlackJack
//
//  Created by Rohit Kotian on 2/1/15.
//  Copyright (c) 2015 Ethical Games. All rights reserved.
//

import Foundation

class Player: NSObject {
    var ID : String
    var name : String
    var isBot: Bool
    
    init(ID: String, name: String, isBot: Bool) {
        self.ID = ID
        self.name = name
        self.isBot = isBot
    }
    
}
