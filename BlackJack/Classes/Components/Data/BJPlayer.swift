//
//  BJPlayer.swift
//  BlackJack
//
//  Created by Rohit Kotian on 2/1/15.
//  Copyright (c) 2015 Ethical Games. All rights reserved.
//

class BJPlayer: Player {
    var hand : Hand = Hand()
    var splitHand : Hand = Hand()
    
    override init(ID: String, name: String, isBot: Bool) {
        super.init(ID: ID, name: name, isBot: isBot)
    }
    
    //TODO: May not be needed to return score values
    func addCard(card : Card) {
        addCard(hand, card: card)
    }
    
    func addCard(card : Card, forSplit : Bool) {
        if (forSplit) {
            addCard(splitHand, card: card)
        } else {
            addCard(hand, card: card)
        }
    }
    
    func split() {
        assert(hand.cards.count == 2, "Split can only happen with two and only two cards")
        let lastCard = hand.removeLast()
        splitHand.append(lastCard)
    }
    
    func reset() {
        hand = Hand()
        splitHand = Hand()
    }
    
    func hasSplit() -> Bool {
        return splitHand.cards.count > 0
    }
    
    func bjScore() -> Int {
        return bjScore(false)
    }
    
    func bjScore(forSplit : Bool) -> Int {
        if (forSplit) {
            return splitHand.score()
        } else {
            return hand.score()
        }
    }
    
    // MARK: - Private helper functions
    private func addCard(hand : Hand, card : Card) {
        hand.append(card)
    }
    
}

class Hand {
    var cards : [Card] = []
    
    func score() -> Int {
        var totalValue = 0
        var hasAnAce = false
        
        for card in cards {
            var cardValue = card.value.rawValue
            if (cardValue > 10) {
                cardValue = 10
            }
            if !hasAnAce && card.value.rawValue == 1 {
                hasAnAce = true
            }
            totalValue += cardValue
        }
        if (hasAnAce && (totalValue + 10 <= BLACKJACK_LIMIT.0)) {
            totalValue += 10
        }
        return totalValue
    }
    
    func append(card : Card) {
        cards.append(card)
    }
    
    func removeLast() -> Card {
        return cards.removeLast()
    }
}