//
//  MenuViewController.swift
//  BlackJack
//
//  Created by Rohit Kotian on 1/31/15.
//  Copyright (c) 2015 Ethical Games. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {

    @IBOutlet weak var btnPlay: UIButton!
    
    @IBOutlet weak var btnOptions: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onPlay(sender: UIButton) {
        NSLog("Play Touched...");
    }
    
    @IBAction func onOptions(sender: UIButton) {
        NSLog("Options touched...");
    }
    
    
}

