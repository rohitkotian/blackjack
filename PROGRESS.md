Completed:
==========
* Initial Start
* Hit event
	* Bust case on hit
* Stand event

* Start again
* 

Progress:
=========
02/21/2015:
* Black jack on deal for the player

02/22/2015:



To Do:
======
02/21/2015:
* Convert _score in BJPlayer as a computed property
* Black jack on deal for the dealer
	* Check for insurance
* Same score on Stand
	* its a tie
* Double down logic
* Split logic
* Surrender logic
* UI Updates, for all cases... BUST, Dealer, etc.

02/22/2015:
* Update UI in case where dealer score is > player score to show dealer wins!
* Update UI in case where dealer score is = player score to show it's a tie!